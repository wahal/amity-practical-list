#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 13                            #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

def isvowel(file1,file2):
    
    vowels = "aeiou"
    
    for vowel in vowels:
        for word in file1.readlines():
            if word.startswith(vowel): pass
            else: file2.write(word)
            
def main():
    
    try:
        file1 = open(raw_input("Enter File Name: "),"rb")
        file2 = open("File2.txt","rb")
        
        isvowel(file1,file2)
        
        file2.read()
    
    except Exception, e: print "Error: " + str(e)
    
    finally:
        print
        os.system("pause")
        
if __name__ == '__main__': main()
    

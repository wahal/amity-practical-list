#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 5                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class PerfectNo:
    
    def __init__(selfie,n):
        
        selfie.n = n
        selfie.factors = []
        
    def Genfactor(selfie):
        
        for element in range(1, selfie.n):
            if selfie.n % element == 0: selfie.factors.append(element)
            else: pass
            
    def perfect(selfie):
        
        if sum(selfie.factors) == selfie.n: print "The Entered Number Is Perfect."
        else: print "The Entered Number Isn't Perfect."
        
    def prime(selfie):
        
        empty = [element for element in selfie.factors]
        if len(empty) == 2: print "The Entered Number Is An Optimus Prime."
        else: print "It's Not A Prime. Better Luck Next Time ! "
        
    def __str__(selfie): print selfie.n, " = ", selfie.factors
    
def main():
    
    try:
        n = input("Enter Your Desired Value: ")
        alpha = PerfectNo(n)
        alpha.Genfactor()
        alpha.perfect()
        alpha.prime()
        alpha.__str__()
    
    except Exception, e: print "Error: " + str(e)
    
    finally:
        
        print
        os.system("pause")
        
if __name__ == '__main__': main()
    
#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 4                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class MyList:
    
    def __init__(selfie): selfie.num = []
    
    def getlist(selfie):
        
        zero = 0
        
        while True:
        
            zero += 1        
            list_input = input("Enter Number %d : " % zero)
            
            if list_input == 0: break
            else: selfie.num.append(list_input)
            
    def printlist(selfie): print selfie.num
    
    def is_num(selfie,x):
        
        if x in selfie.num: return True
        else: return False
        
    def max_min(selfie):
        
        maximum = max(selfie.num)
        minimum = min(selfie.num)
        
        print "\nHighest Number in The List: ", maximum
        print "Lowest Number In The List: ", minimum
        print "Difference Betwixt The Highest & Lowest: ", (maximum - minimum)
        
    def closest(selfie,v):
        
        empty = []
        for element in selfie.num:
            empty.append(abs(element-v))
            
        min_index = empty.index(min(empty))
        
        print "Closest Element To The Value Of 'V' In The List Is: ", selfie.num[min_index]
        
def main():
    
    try:
        alpha = MyList()
        alpha.getlist()
        alpha.printlist()
        
        num = input("\nEnter Number To Cross Check: ")
        print alpha.is_num(num)
        alpha.max_min()
        
        close = input("Input To Check The Closest Value: ")
        alpha.closest(close)
    
    except Exception, e: print "Error: " + str(e)
    
    finally:
        
        print
        os.system("pause")
        
if __name__ == '__main__': main()
    
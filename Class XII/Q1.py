#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 1                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class Quadratic:
    
    def __init__(selfie,a,b,c):
        
        selfie.a = a
        selfie.b = b
        selfie.c = c
        
    calc = lambda selfie,x: selfie.a*(x**2) + selfie.b*x + selfie.c
    
    def roots(selfie):
        
        disc = selfie.b**2 - 4*selfie.a*selfie.c
    
        if disc < 0: print "This Equation Has No Real Roots."
        elif disc == 0:
            x = (-selfie.b+((selfie.b**2-4*selfie.a*selfie.c))**(0.5))/2*selfie.a
            print "This Equation Has Only One Root: ", x
        else:
            x1 = (-selfie.b+(((selfie.b**2)-(4*(selfie.a*selfie.c))))**(0.5))/(2*selfie.a)
            x2 = (-selfie.b-(((selfie.b**2)-(4*(selfie.a*selfie.c))))**(0.5))/(2*selfie.a)
            print "This equation has two solutions: ", x1, " or", x2
        
def main():
        
    print "If Base Equation Is Of The Form Ax^2 + Bx + C = y"
    
    
    try:
        a = input("\nEnter The Value Of A: ")
        b = input("Enter The Value Of B: ")
        c = input("Enter The Value Of C: ")
        x = input("Enter The Value Of X: ")
        
        alpha = Quadratic(a,b,c)
        print "\nThe Required Value Of Y: ", alpha.calc(x)
        alpha.roots()
        
    except Exception, e: print "Error: " + str(e)    
    
    finally:
        print
        os.system("pause")
    
if __name__ == '__main__': main()
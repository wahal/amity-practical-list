class Stack(object):
    
    def __init__(self,l): self.items = l
            
    def __iter__(self): return self        
    
    def peep(self):
        
        try:
            for step in range(len(self.items)):
                location_of_smallest = step
                for location in range(step, len(self.items)):
                    
                    if self.items[location] < self.items[location_of_smallest]:
                        location_of_smallest = location
                
                temporary_item = self.items[step]
                self.items[step] = self.items[location_of_smallest]
                self.items[location_of_smallest] = temporary_item
                    
        except IndexError: pass
        
        finally: return self.items
        
    def give(self):
        
        for x in self.items[::-1]: print x, ",",
        print "\\"
        
def main():
    
    l1 = raw_input("Enter Diameters With Commas: ").split(",")
    l = [int(x) for x in l1]
    x = Stack(l)
    print x.peep()
    x.give()
    
if __name__ == '__main__': main()
        
    

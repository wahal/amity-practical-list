class myStack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop(0)

     def size(self):
         return len(self.items)

s = myStack()
s.push('1')
s.push('2')
print(s.pop())
print s